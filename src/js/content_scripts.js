chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
  if (request.type === 'QRMAN_ASK_URL') {
    sendResponse({ url: document.URL });
  }
});
