import React from 'react';
import QRCode from 'qrcode.react';

export default class App extends React.Component {
  constructor(prop) {
    super(prop);

    this.state = {
      url: ''
    };
  }

  componentWillMount() {
    chrome.tabs.query({active: true, currentWindow: true}, tabs => {
      chrome.tabs.sendMessage(tabs[0].id, { type: 'QRMAN_ASK_URL' }, res => {
        if (typeof res !== 'undefined') {
          this.setState({ url: res.url });
        }
      });
    });
  }

  renderQRCode() {
    if (this.state.url !== '') {
      return (
        <QRCode value={this.state.url} size={160}
                bgColor="#F8F8F8" fgColor="#0A3942" />
      );
    }
  }

  render() {
    return (
      <div className="container">
        <h1>QRMAN</h1>
        <div className="code">
          <div className="code-inner">
            {this.renderQRCode()}
          </div>
        </div>
      </div>
    );
  }
}
